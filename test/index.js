const chai = require('chai');
const sameOld = require('../');

const expect = chai.expect;

describe('Same Old', function () {

    it('should throw on attempts to mutate objects', function () {

        const test = sameOld({ foo: 'test' });
        const change = () => { test.foo = 'bar'; };

        expect(change).to.throw(sameOld.ImmutabilityError);
        expect(test).to.deep.equal({ foo: 'test' });
    });

    it('should throw on attempts to mutate arrays', function () {

        const test = sameOld([1, 2, 3]);
        const change = () => { test.push(4); };

        expect(change).to.throw(sameOld.ImmutabilityError);
        expect(test).to.deep.equal([1, 2, 3]);
        expect(test).to.have.ordered.members([1, 2, 3]);
    });

    it('should throw on attempts to mutate nested objects', function () {

        const test = sameOld({ foo: 'test', nested: { bar: 'test' } });
        const change = () => { test.nested.bar = 'bar'; };

        expect(change).to.throw(sameOld.ImmutabilityError);
        expect(test).to.deep.equal({ foo: 'test', nested: { bar: 'test' } });
    });

    it('should make function return values immutable', function () {

        const test = sameOld(() => [1, 2, 3]);
        const change = () => { test().push(4); };

        expect(change).to.throw(sameOld.ImmutabilityError);
        expect(test()).to.deep.equal([1, 2, 3]);
        expect(test()).to.have.ordered.members([1, 2, 3]);
    });

    it('should cache input objects', function () {

        const test = { foo: 'test' };
        sameOld(test);

        expect(sameOld.getCache().has(test)).to.be.true;
    });

    it('should allow non-mutative array operations', function () {

        const test = sameOld([1, 2, 3]);
        const test2 = test.concat([4]);

        expect(test2).to.have.ordered.members([1, 2, 3, 4]);
    });

    it('should allow non-mutative array iteration', function () {

        const test = sameOld([1, 2, 3]);
        const test2 = test.map(el => el * 2);

        expect(test2).to.have.ordered.members([2, 4, 6]);
    });

    it('should return immutable arrays from non-mutative array operations', function () {

        const test = sameOld([1, 2, 3]);
        const test2 = test.concat([4]);
        const change = () => { test2.push(5); };

        expect(change).to.throw(sameOld.ImmutabilityError);
        expect(test2).to.deep.equal([1, 2, 3, 4]);
        expect(test2).to.have.ordered.members([1, 2, 3, 4]);
    });

    it('should return immutable arrays from non-mutative array iteration', function () {

        const test = sameOld([1, 2, 3]);
        const test2 = test.map(el => el * 2);
        const change = () => { test2.push(8); };

        expect(change).to.throw(sameOld.ImmutabilityError);
        expect(test2).to.deep.equal([2, 4, 6]);
        expect(test2).to.have.ordered.members([2, 4, 6]);
    });

    it('should allow array iteration via Array Iterator', function () {

        const test = sameOld([1, 2, 3]);
        const iterate = () => [...test];

        expect(iterate).to.not.throw();
        expect(iterate()).to.deep.equal([1, 2, 3]);
        expect(iterate()).to.have.ordered.members([1, 2, 3]);
    });

    it('should provide mutable version of object via toJS()', function () {

        const test = sameOld({ foo: 'test' });
        const test2 = test.toJS();

        test2.foo = 'changed';

        expect(test2).to.deep.equal({ foo: 'changed' });
        expect(test).to.deep.equal({ foo: 'test' });
    });

    it('should provide mutable version of array via toJS()', function () {

        const test = sameOld([1, 2, 3]);
        const test2 = test.toJS();

        test2.push(4);

        expect(test2).to.deep.equal([1, 2, 3, 4]);
        expect(test).to.deep.equal([1, 2, 3]);
    });
});
