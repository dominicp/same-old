/**
 * A super simple and lightweight module for providing immutable objects via Proxies. Baesed on the
 * Mozilla Hacks blog referenced below.
 *
 * @see https://hacks.mozilla.org/2015/07/es6-in-depth-proxies-and-reflect/
 */

// Define a custom error to make stack traces nicer
class ImmutabilityError extends Error {

    constructor(message) {
        super(message);

        this.name = this.constructor.name;
    }
}

// Define a function to throw on mutation attempts
const accessError = () => {

    throw new ImmutabilityError('Cannot modify immutable object.');
};

// Setup a cache so we don't proxy the same object twice
const cache = new WeakMap();

/**
 * This function returns an immutable version of the passed in object.
 *
 * @param  {Mixed} target An object or an array to make immutable. If a function is passed, any returned
 *                        value of that funciton will be made immutable.
 * @return {Mixed}        The immutable version of the passed in object or array.
 */
const sameOld = (input) => {

    // Only proxy objects, just return literals
    if (Object(input) !== input) { return input; }

    if (cache.has(input)) { return cache.get(input); }

    // Setup a copy object
    let copy = null;

    if (typeof input === 'function') {

        copy = () => {};

    } else {

        copy = Array.isArray(input) ? [] : {};
    }

    // Define a mutable version of the input
    const mutable = new Proxy(input, {

        set(target, key, value) {

            return Reflect.set(copy, key, value, copy);
        },

        get(target, key) {

            let result = Reflect.get(copy, key);

            if (! result) { result = Reflect.get(target, key); }

            return result;
        }
    });

    // Define the proxy object
    const proxy = new Proxy(input, {

        // Override all five mutating methods.
        set: accessError,
        defineProperty: accessError,
        deleteProperty: accessError,
        preventExtensions: accessError,
        setPrototypeOf: accessError,

        // Handle function calls. We want all results to be immutable.
        apply(target, thisArg, argumentsList) {

            const result = Reflect.apply(target, thisArg, argumentsList);

            return sameOld(result);
        },

        // Wrap other results in read-only views.
        get(target, key, receiver) {

            if (key === 'toJS') { return () => mutable; }

            // Special case for array iterators
            // See https://stackoverflow.com/a/41047894/931860
            if (key === Symbol.iterator) { return target[Symbol.iterator].bind(target); }

            // Start by just doing the default behavior.
            const result = Reflect.get(target, key, receiver);

            // Special case for prototype otherwise we get invariant errors
            // see also getOwnPropertyDescriptor
            if (key === 'prototype') { return result; }

            // Make sure not to return a mutable object!
            return sameOld(result);
        },

        getPrototypeOf(target) {

            const proto = Reflect.getPrototypeOf(target);

            return sameOld(proto);
        },

        getOwnPropertyDescriptor(target, key) {

            const result = Reflect.getOwnPropertyDescriptor(target, key);

            // Special case for prototype otherwise we get invariant errors
            // see also get
            if (! result || key === 'prototype') { return result; }

            result.writable = false;
            result.value = sameOld(result.value);

            return result;
        }
    });

    cache.set(input, proxy);

    return proxy;
};

// Provide access to the cache and our custom error for testing purposes
sameOld.getCache = () => cache;
sameOld.ImmutabilityError = ImmutabilityError;

module.exports = sameOld;
